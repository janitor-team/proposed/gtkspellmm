gtkspellmm (3.0.5+dfsg-3) UNRELEASED; urgency=medium

  * d/watch: Use https protocol

 -- Ondřej Nový <onovy@debian.org>  Mon, 01 Oct 2018 10:43:58 +0200

gtkspellmm (3.0.5+dfsg-2) unstable; urgency=medium

  * Remove Build-Depend on dh-autoreconf
    - debhelper >= 10 does the magic automatically
  * Remove unused lintian override
  * Update Vcs URLs in d/control to point to salsa.d.o
  * Use uscans ability to repack the source tarball
  * Add upstream metadata
  * Bump debhelper compat level to 11 (no changes necessary)
  * Bump Standards-Version to 4.1.4 (no changes necessary)

 -- Philip Rinn <rinni@inventati.org>  Sun, 29 Apr 2018 16:23:09 +0200

gtkspellmm (3.0.5+dfsg-1) unstable; urgency=medium

  * New upstream release:
    - Bump build-dependency libgtkspell3-3-dev (>= 3.0.9)
    - Add new symbols to symbols file
  * Add build-dependency graphiz
  * Update debian/{watch, get-orig-source.sh}, upstream uses xz compression now
  * Mark -doc package Multi-Arch: foreign
  * Bump debhelper compat from 9 to 10 (no changes necessary).

 -- Philip Rinn <rinni@inventati.org>  Thu, 27 Oct 2016 19:51:38 +0200

gtkspellmm (3.0.4+dfsg-1) unstable; urgency=medium

  * New upstream release:
    - Bump build-dependency libgtkspell3-3-dev (>= 3.0.8)
    - Add new symbols to symbols file
  * Mark symbol as optional that's missing when built with -O3 (closes: #798459)
  * Use secure URIs for vcs-* in debian/control
  * remove patch 01-doxygen-segfault.patch:
    - Doxygen doesn't segfault anymore
  * Remove patch 02-doxygen-no-timestamps.patch:
    - Applied upstream
  * Bump Standards-Version to 3.9.8 (no changes necessary)

 -- Philip Rinn <rinni@inventati.org>  Wed, 20 Apr 2016 15:39:31 +0200

gtkspellmm (3.0.3+dfsg-2) unstable; urgency=medium

  * Rename library for g++-5 transition (closes: #796874):
    - Add Conflicts/Replaces for the old library
    - Bump build-dependencies to g++-5-transitioned versions
  * Bump Standards-Version to 3.9.6 (no changes necessary)
  * Generate docs without timestamps to make the build reproducible

 -- Philip Rinn <rinni@inventati.org>  Tue, 25 Aug 2015 18:24:10 +0200

gtkspellmm (3.0.3+dfsg-1) unstable; urgency=low

  * Initial release (closes: #748352)

 -- Philip Rinn <rinni@inventati.org>  Sun, 24 Aug 2014 12:31:54 +0200
